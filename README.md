# 影院管理系统的设计与实现

#### 项目介绍
一、	系统目标
本系统主要目标是实现影院信息的在线管理，用户可以通过系统查看影院近期电影的上映情况，查看电影和影院评分，预订座位。用户观影后可以填写影评，与其他观影人进行交流。影院管理人员负责人电影和影院信息的管理，管理影评，统计影院收入等。
二、	核心业务需求
调研网上同类系统功能，要求本系统包括2种角色：用户、管理员。
用户通过系统进行影院和电影信息的查询、预订座位、查看并填写影评。管理员管理电影、影院、影评信息、统计影院收入等。
三、	技术路线
开发框架使用SSM三大框架技术，前端界面采用HTML5语言设计，采用JSP动态网站技术来进行网络建站开发工作。开发工具使用Eclipse，Web服务器使用Tomcat，数据库服务器使用MySQL，系统架构采用B/S架构。也可采用Android开发手机App，开发工具使用Android Studio，数据库使用SQLite。
四、	论文撰写
本系统采用面向对象方法对系统进行分析与设计，并使用规范的UML图、表和专业术语来描述系统分析、设计、实现、测试过程与模型。并按照软件工程专业论文模版来撰写论文。
五、	参考文献
要求查阅近三年为主的相关

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)